import xarray as xr

data = xr.open_dataset(f'data/1-input/adaptor.mars.internal-1663768901.8738832-7627-11-ae155e25-b1d3-4f5d-b250-aabdf976187e.nc')

#percipitation for a pixel near amsterdam
data['tp'].sel(longitude=4.5, latitude=51.9, method="nearest").sel(time=slice("1959-01-01","1959-12-31")).sum()
data['tp'].sel(longitude=4.5, latitude=51.9, method="nearest").sel(time=slice("2021-01-01","2021-12-31")).sum()

