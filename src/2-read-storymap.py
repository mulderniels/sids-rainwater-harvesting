from pyexpat.errors import XML_ERROR_UNCLOSED_TOKEN
import xarray as xr
import matplotlib.pyplot as plt
import geopandas as gpd
import pandas as pd
from sklearn.linear_model import LinearRegression

data = gpd.read_file('data/1-input/storymap/buffers31/buffers31.shp')

def makeplot(x, y, x_label, y_label):
    """
    #the data
    x_col = 'F_Total_HH'
    y_col = 'total_sto'

    x = data[x_col]
    y = data[y_col]
    """

    #remove nans
    df = pd.DataFrame([x,y], index=['x', 'y']).astype(float).T.dropna(axis=0)
    x = df['x'].values
    y = df['y'].values
    n = df.size

    #curve fitting
    model = LinearRegression().fit(x.reshape((-1,1)),y)
    fit_x = [min(x), max(x)]
    fit_y = [model.intercept_+model.coef_[0]*i for i in fit_x]

    #plot
    fig, ax = plt.subplots(1,1)
    ax.scatter(x, y)
    ax.plot(fit_x, fit_y, 'k--')
    ax.set_xlabel(x_label)
    ax.set_ylabel(y_label)
    ax.set_title(f'n={n}')

    fig.show()
    fig.savefig(f'data/2-interim/plots/{x_label} vs {y_label}.png')
    print(f'{model.intercept_}-{model.coef_[0]}')

makeplot(data['F_Total_HH'], data['F_Total_po'], 'total households', 'total population')
makeplot(data['F_Total_HH'], data['total_sto'], 'total households', 'total storage')
makeplot(data['F_Avg_annu'], data['total_sto'], 'avg annual rainfall (mm)', 'total storage')
makeplot(data['F_Avg_annu'], data['total_sto'].astype(float)/data['F_Total_po'].astype(float), 'avg annual rainfall (mm)', 'total storage per capita')


"""
wat weten we van elk eiland
- neerslag (bijna, hopelijk)
- population
- surface area
- gdp (misschien?)

wat weten we van de fancy eilanden
- storage capacity

wat willen we weten van elk eiland
- zoetewaterbeschikbaarheid, ofwel: 
    harvestable rainwater + extractable ground water

"""