import imod
import xarray as xr
import numpy as np
import geopandas as gpd
import matplotlib.pyplot as plt
#import pandas as pd
#from pathlib import Path

data = xr.open_dataset(f'data/1-input/adaptor.mars.internal-1663768901.8738832-7627-11-ae155e25-b1d3-4f5d-b250-aabdf976187e.nc')

shp_path = 'data/1-input/shp/global_sids_fixed_geoms_point.shp'
shp = gpd.read_file(shp_path).to_crs(epsg=4326)
shp_bbox = shp.geometry.total_bounds

dx = np.diff(data.longitude)[0]
dy = np.diff(data.latitude)[0]

data1 = xr.concat([
    data.sel(longitude=slice(180, 359.75)),
    data.sel(longitude=slice(0, 179.75))
    #data.sel(longitude=slice(180, 181)),
    #data.sel(longitude=slice(0, 1))
], dim='longitude')

data1['longitude'] = np.linspace(-180.00,179.75,360*4)

mock_data = data1.isel(time=0).drop('time').sel(
    longitude=slice(shp_bbox[0]-dx,shp_bbox[2]+dx),
    latitude=slice(shp_bbox[3]-dy,shp_bbox[1]+dy)
    )
mock_data.to_netcdf('data/2-interim/mockdata_lng_lat.nc')
mock_data = mock_data.rename({'longitude':'x','latitude':'y'})
mock_data.to_netcdf('data/2-interim/mockdata_x_y.nc')

data1 = data1.rename({'longitude':'x','latitude':'y'})

shp1 = shp[
    (shp.geometry.x>data1.x.min().values) &
    (shp.geometry.x<data1.x.max().values) &
    (shp.geometry.y>data1.y.min().values) &
    (shp.geometry.y<data1.y.max().values)
    ]

pointdata = imod.select.points_values(data1, x=shp1.geometry.x, y=shp1.geometry.y)

#average in mm per day
historic_average = pointdata.sel(time=slice("01-01-2010", "12-31-2020")).mean(dim="time")
historic_average_df = historic_average.to_pandas()
shp['percipitation_m_per_day'] = historic_average_df['tp']

shp['hh'] = shp['worldpop']/6.7
shp['rainwater_harvest_m3_per_day'] = shp['hh']*50*shp['percipitation_m_per_day']
shp['rainwater_harvest_l_per_capita_per_day'] = shp['rainwater_harvest_m3_per_day']*1000/shp['worldpop']

shp['rainwater_harvest_l_per_capita_per_day'].plot.hist()

shp.to_file('data/3-results/points.shp')