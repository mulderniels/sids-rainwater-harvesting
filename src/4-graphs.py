import geopandas as gpd
import numpy as np
import matplotlib as mpl
import matplotlib.pylab as plt

points = gpd.read_file('data/3-results/points.shp')
bounds = [0,25,38,50,75,100,175]

cmap = plt.cm.RdYlGn
cmaplist = [cmap(i) for i in range(cmap.N)]
cmap = mpl.colors.LinearSegmentedColormap.from_list('Custom cmap', cmaplist, cmap.N)
norm = mpl.colors.BoundaryNorm(bounds, cmap.N)
cbar = plt.cm.ScalarMappable(norm=norm, cmap=cmap)

## map

world = gpd.read_file(gpd.datasets.get_path('naturalearth_lowres'))
points = points.to_crs(world.crs)

fig, ax = plt.subplots(figsize=(15, 7))
world.plot(ax=ax, color='white', edgecolor='black', linewidth=0.1)
points.plot(ax=ax, marker='o', column='rainwate_1', cmap=cmap, norm=norm, markersize=3)

ax_cbar = fig.colorbar(cbar, ax=ax)
ax_cbar.set_label('Liter per capita per day')
ax.set_title('Harvestable Rain Water')

## Histogram

counts, edges = np.histogram(points['rainwate_1'], bins=bounds)
x = np.arange(len(counts))+1

labels = ['']
for i in x:
    labels.append(f'{bounds[i-1]}-{bounds[i]}')

fig, ax = plt.subplots()
bars = ax.bar(x, counts, color=cmap(x/len(x)))
ax.bar_label(bars)
ax.set_xticklabels(labels)
ax.set_xlabel('Liter per capita per day')
ax.set_ylabel('Number of SIDS areas')
ax.set_title('Harvestable Rain Water')


